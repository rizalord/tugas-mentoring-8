<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url', 'form', 'download']);
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data = $this->getData();
		$this->load->view('welcome_message', ['data' => $data]);
	}

	public function export()
	{
		// download file
		force_download('public/data.xlsx', null);
	}

	public function import()
	{
		$config['upload_path'] = './public/';
		$config['allowed_types'] = 'xls|xlsx|csv';
		$config['max_size'] = 10000;

		$this->load->library('upload', $config);

		// upload and then read file
		if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();

			// validate file extension
			$objPHPExcel = null;

			if ($data['file_ext'] == '.xls') {
				$objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
			} else if ($data['file_ext'] == '.xlsx') {
				$objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
			} else if ($data['file_ext'] == '.csv') {
				$objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Csv');
			}

			if ($objPHPExcel == null) {
				echo "Invalid file extension";
				return;
			}

			// read file
			$objPHPExcel = $objPHPExcel->load($data['full_path']);
			$sheet = $objPHPExcel->getActiveSheet();

			// get max row
			$highestRow = $sheet->getHighestRow();

			// get max column
			$highestColumn = $sheet->getHighestColumn();

			// get max column index
			$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

			// validate cell A1 must be 'Nama Mahasiswa'
			if ($sheet->getCell('A1')->getValue() != 'Nama Mahasiswa') {
				echo "Invalid file format, cell A1 must be 'Nama Mahasiswa'";
				unlink($data['full_path']);
				return;
			}

			// validate cell A2 must be 'Fakultas'
			if ($sheet->getCell('A2')->getValue() != 'Fakultas') {
				echo "Invalid file format, cell A2 must be 'Fakultas'";
				unlink($data['full_path']);
				return;
			}

			// validate cell A3 must be 'Prodi'
			if ($sheet->getCell('A3')->getValue() != 'Prodi') {
				echo "Invalid file format, cell A3 must be 'Prodi'";
				unlink($data['full_path']);
				return;
			}

			// validate cell A4 must be 'No telpon'
			if ($sheet->getCell('A4')->getValue() != 'No telpon') {
				echo "Invalid file format, cell A4 must be 'No telpon'";
				unlink($data['full_path']);
				return;
			}

			// validate cell A5 must be 'Jenis kelamin'
			if ($sheet->getCell('A5')->getValue() != 'Jenis kelamin') {
				echo "Invalid file format, cell A5 must be 'Jenis kelamin'";
				unlink($data['full_path']);
				return;
			}

			// validate cell A6 must be 'Alamat'
			if ($sheet->getCell('A6')->getValue() != 'Alamat') {
				echo "Invalid file format, cell A6 must be 'Alamat'";
				unlink($data['full_path']);
				return;
			}

			// validate cell A7 must be 'Tanggal Lahir'
			if ($sheet->getCell('A7')->getValue() != 'Tanggal Lahir') {
				echo "Invalid file format, cell A7 must be 'Tanggal Lahir'";
				unlink($data['full_path']);
				return;
			}

			// get data
			$realData = [];

			for ($row = 1; $row <= $highestRow; ++$row) {
				for ($col = 1; $col <= $highestColumnIndex; ++$col) {
					$realData[$row][] = $sheet->getCell(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col) . $row)->getValue();
				}
			}

			// save file as 'public/data.xlsx'
			$writer = new Xlsx($objPHPExcel);
			$writer->save('public/data.xlsx');

			// delete uploaded file
			unlink($data['full_path']);

			// show data
			$this->load->view('read_import', ['data' => $realData]);
		} else {
			echo $this->upload->display_errors();
		}
	}

	private function getData()
	{
		// import data from 'public/import_data.xlsx'
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('public/data.xlsx');

		// get active sheet
		$activeSheet = $spreadsheet->getActiveSheet();

		// get max row
		$highestRow = $activeSheet->getHighestRow();

		// get max column
		$highestColumn = $activeSheet->getHighestColumn();

		// get max column index
		$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

		// get data
		$data = [];

		for ($row = 1; $row <= $highestRow; ++$row) {
			for ($col = 1; $col <= $highestColumnIndex; ++$col) {
				$data[$row][] = $activeSheet->getCell(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col) . $row)->getValue();
			}
		}

		return $data;
	}
}
